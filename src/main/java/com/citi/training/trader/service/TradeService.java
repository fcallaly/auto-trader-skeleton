package com.citi.training.trader.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.trader.dao.TradeDao;
import com.citi.training.trader.model.Trade;

@Component
public class TradeService {

    @Autowired
    private TradeDao tradeDao;

    public int save(Trade trade) {
        return tradeDao.save(trade);
    }

    public List<Trade> findAll() {
        return tradeDao.findAll();
    }

    public Trade findById(int id) {
        return tradeDao.findById(id);
    }

    public List<Trade> findAllByState(Trade.TradeState state) {
        return tradeDao.findAllByState(state);
    }

    public Trade findLatestByStrategyId(int strategyId) {
        return tradeDao.findLatestByStrategyId(strategyId);
    }

    public void deleteById(int id) {
        tradeDao.deleteById(id);
    }
}
